package kz.aitu.Crud.repository;


import kz.aitu.Crud.model.SearchKeyRouting;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface SearchKeyRoutingRepository extends CrudRepository<SearchKeyRouting, Long> {
    SearchKeyRouting getSearchKeyRouting();
    SearchKeyRouting findById(long id);

    void updateTableNameByID(String tableName, long id);
}

