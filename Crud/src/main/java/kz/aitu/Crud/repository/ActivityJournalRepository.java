package kz.aitu.Crud.repository;


import kz.aitu.Crud.model.ActivityJournal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;




@Repository
public interface ActivityJournalRepository extends CrudRepository<ActivityJournal, Long> {
   ActivityJournal getActivityJournal();
   ActivityJournal findById(long id);

   void updateEventTypeByID(String eventType, long id);
}

