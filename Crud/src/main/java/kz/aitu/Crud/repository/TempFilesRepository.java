package kz.aitu.Crud.repository;

import kz.aitu.Crud.model.TempFiles;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface TempFilesRepository extends CrudRepository<TempFiles, Long> {
    TempFiles getTempFiles();
    TempFiles findById(long id);

    void updateFileBinaryByteByID(short fileBinaryByte, long id);
}

