package kz.aitu.Crud.repository;


import kz.aitu.Crud.model.File;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface FileRepository extends CrudRepository<File, Long> {
    File getFile();
    File findById(long id);

    void updateNameByID(String name, long id);
}

