package kz.aitu.Crud.repository;

import kz.aitu.Crud.model.CaseIndex;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface CaseIndexRepository extends CrudRepository<CaseIndex, Long> {
CaseIndex getCaseIndex();
CaseIndex findById(long id);

    void updateTitleEnByID(String titleEN, long id);
}

