package kz.aitu.Crud.repository;

import kz.aitu.Crud.model.Record;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface RecordRepository extends CrudRepository<Record, Long> {
    Record getRecord();
    Record findById(long id);

    void updateNumberByID(String number, long id);
}

