package kz.aitu.Crud.repository;

import kz.aitu.Crud.model.HistoryRequestStatus;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface HistoryRequestStatusRepository extends CrudRepository<HistoryRequestStatus, Long> {
    HistoryRequestStatus getHistoryRequestStatus();
    HistoryRequestStatus findById(long id);

    void updateStatusByID(String status, long id);
}

