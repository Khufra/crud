package kz.aitu.Crud.repository;


import kz.aitu.Crud.model.Authorization;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface AuthorizationRepository extends CrudRepository<Authorization, Long> {
    Authorization findById(long id);
    Authorization getAuthorization();

    void updateUsernameByID(String username, long id);
}

