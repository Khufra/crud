package kz.aitu.Crud.repository;

import kz.aitu.Crud.model.CatalogCase;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CatalogCaseRepository extends CrudRepository<CatalogCase, Long> {
    CatalogCase getCatalogCase();
    CatalogCase findById(long id);

    void updateCreatedByByID(long createdBy, long id);
}

