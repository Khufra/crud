package kz.aitu.Crud.repository;


import kz.aitu.Crud.model.DestructionAct;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface DestructionActRepository extends CrudRepository<DestructionAct, Long> {
    DestructionAct getDestructionAct();
    DestructionAct findById(long id);

    void updateBaseByID(String base, long id);
}

