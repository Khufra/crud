package kz.aitu.Crud.repository;

import kz.aitu.Crud.model.NomenclatureSummary;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface NomenclatureSummaryRepository extends CrudRepository<NomenclatureSummary, Long> {
    NomenclatureSummary getNomenclatureSummary();
    NomenclatureSummary findById(long id);

    void updateYearByID(int year, long id);
}

