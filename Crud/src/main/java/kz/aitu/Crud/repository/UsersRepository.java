package kz.aitu.Crud.repository;


import kz.aitu.Crud.model.Users;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UsersRepository extends CrudRepository<Users, Long> {
    Users getUsers();
    Users findById(long id);

    void updateNameByID(String name, long id);
}

