package kz.aitu.Crud.repository;

import kz.aitu.Crud.model.Notification;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface NotificationRepository extends CrudRepository<Notification, Long> {
    Notification getNotification();
    Notification findById(long id);

    void updateObjectTypeByID(String objectType, long id);
}

