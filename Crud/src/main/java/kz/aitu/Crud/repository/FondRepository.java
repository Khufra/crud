package kz.aitu.Crud.repository;

import kz.aitu.Crud.model.Fond;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface FondRepository extends CrudRepository<Fond, Long> {
    Fond getFond();
    Fond findById(long id);

    void updateFondNumberByID(String fondNumber, long id);
}

