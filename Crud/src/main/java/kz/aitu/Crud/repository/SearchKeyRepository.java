package kz.aitu.Crud.repository;


import kz.aitu.Crud.model.SearchKey;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface SearchKeyRepository extends CrudRepository<SearchKey, Long> {
    SearchKey getSearchKey();
    SearchKey findById(long id);

    void updateNameByID(String name, long id);
}

