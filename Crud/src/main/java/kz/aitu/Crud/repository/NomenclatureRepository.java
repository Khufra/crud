package kz.aitu.Crud.repository;

import kz.aitu.Crud.model.Nomenclature;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface NomenclatureRepository extends CrudRepository<Nomenclature, Long> {
    Nomenclature getNomenclature();
    Nomenclature findById(long id);

    void updateYearByID(int year, long id);
}

