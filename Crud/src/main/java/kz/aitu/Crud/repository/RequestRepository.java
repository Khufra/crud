package kz.aitu.Crud.repository;

import kz.aitu.Crud.model.Request;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


        @Repository
public interface RequestRepository extends CrudRepository<Request, Long> {
    Request getRequest();
    Request findById(long id);

            void updateRequestUserIdByID(long requestUserId, long id);
        }
