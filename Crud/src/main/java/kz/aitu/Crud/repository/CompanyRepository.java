package kz.aitu.Crud.repository;


import kz.aitu.Crud.model.Company;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface CompanyRepository extends CrudRepository<Company, Long> {
    Company getCompany();
    Company findById(long id);

    void updateNameEnByID(String nameEN, long id);
}

