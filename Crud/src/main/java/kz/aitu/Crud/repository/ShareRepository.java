package kz.aitu.Crud.repository;


import kz.aitu.Crud.model.Share;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface ShareRepository extends CrudRepository<Share, Long> {
    Share getShare();
    Share findById(long id);

    void updateNoteByID(String note, long id);
}

