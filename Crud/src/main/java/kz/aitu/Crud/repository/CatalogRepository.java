package kz.aitu.Crud.repository;


import kz.aitu.Crud.model.Catalog;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface CatalogRepository extends CrudRepository<Catalog, Long> {
    Catalog getCatalog();
    Catalog findById(long id);

    void updateNameEnByID(String nameEN, long id);
}

