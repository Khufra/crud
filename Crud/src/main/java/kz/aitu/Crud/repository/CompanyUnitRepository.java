package kz.aitu.Crud.repository;


import kz.aitu.Crud.model.CompanyUnit;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CompanyUnitRepository extends CrudRepository<CompanyUnit, Long> {
    CompanyUnit getCompanyUnit();
    CompanyUnit findById(long id);

    void updateNameEnByID(String nameEN, long id);
}

