package kz.aitu.Crud.repository;


import kz.aitu.Crud.model.Case;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface CaseRepository extends CrudRepository<Case, Long>  {
    Case getCase();
    Case findById(long id);

    void updateCaseHeadingEnByID(String caseHeadingEN, long id);
}

