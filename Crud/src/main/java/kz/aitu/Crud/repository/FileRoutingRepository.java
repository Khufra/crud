package kz.aitu.Crud.repository;

import kz.aitu.Crud.model.FileRouting;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface FileRoutingRepository extends CrudRepository<FileRouting, Long> {
    FileRouting getFileRouting();
    FileRouting findById(long id);

    void updateTableNameByID(String tableName, long id);
}

