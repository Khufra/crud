package kz.aitu.Crud.repository;

import kz.aitu.Crud.model.Location;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface LocationRepository extends CrudRepository<Location, Long> {
    Location getLocation();
    Location findById(long id);

    void updateBoxByID(String box, long id);
}

