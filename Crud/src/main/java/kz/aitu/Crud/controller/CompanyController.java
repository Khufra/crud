package kz.aitu.Crud.controller;

import kz.aitu.Crud.service.CompanyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CompanyController {
    private final CompanyService companyService;

    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @GetMapping("/crud/companies")
    public ResponseEntity<?> getCompanies() {
        return ResponseEntity.ok(companyService.getAll());
    }

    @GetMapping("/crud/company/{id}")
    public ResponseEntity<?> findCompanyByID(@PathVariable long id){
        return ResponseEntity.ok(companyService.findByID(id));
    }

    @DeleteMapping("/crud/deleteCompany/{id}")
    public void deleteCompanyByID(@PathVariable long id){
        companyService.deleteByID(id);
    }

    @RequestMapping(value = "/crud/updateCompany/{id}/{nameEN}", method = RequestMethod.GET)
    public void updateCompanyByID(@PathVariable("id") long id, @PathVariable("titleEN") String nameEN){
        companyService.updateByID(id, nameEN);
    }
}

