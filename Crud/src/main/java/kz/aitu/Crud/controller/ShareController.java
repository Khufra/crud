package kz.aitu.Crud.controller;

import kz.aitu.Crud.service.ShareService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ShareController {
    private final ShareService shareService;

    public ShareController(ShareService shareService) {
        this.shareService = shareService;
    }

    @GetMapping("/crud/shares")
    public ResponseEntity<?> getShares() {
        return ResponseEntity.ok(shareService.getAll());
    }

    @GetMapping("/crud/share/{id}")
    public ResponseEntity<?> findShareByID(@PathVariable long id){
        return ResponseEntity.ok(shareService.findByID(id));
    }

    @DeleteMapping("/crud/deleteShare/{id}")
    public void deleteShareByID(@PathVariable long id){
        shareService.deleteByID(id);
    }

    @RequestMapping(value = "/crud/updateShare/{id}/{note}", method = RequestMethod.GET)
    public void updateShareByID(@PathVariable("id") long id, @PathVariable("note") String note){
        shareService.updateByID(id, note);
    }
}

