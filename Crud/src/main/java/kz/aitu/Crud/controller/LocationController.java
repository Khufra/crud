package kz.aitu.Crud.controller;

import kz.aitu.Crud.service.LocationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class LocationController {
    private final LocationService locationService;

    public LocationController(LocationService locationService) {
        this.locationService = locationService;
    }

    @GetMapping("/crud/locations")
    public ResponseEntity<?> getLocations() {
        return ResponseEntity.ok(locationService.getAll());
    }

    @GetMapping("/crud/location/{id}")
    public ResponseEntity<?> findLocationByID(@PathVariable long id){
        return ResponseEntity.ok(locationService.findByID(id));
    }

    @DeleteMapping("/crud/deleteLocation/{id}")
    public void deleteLocationByID(@PathVariable long id){
        locationService.deleteByID(id);
    }

    @RequestMapping(value = "/crud/updateLocation/{id}/{box}", method = RequestMethod.GET)
    public void updateLocationByID(@PathVariable("id") long id, @PathVariable("box") String box){
        locationService.updateByID(id, box);
    }
}

