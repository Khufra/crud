package kz.aitu.Crud.controller;


import kz.aitu.Crud.service.AuthorizationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class AuthorizationController {
    private final AuthorizationService authorizationService;

    public AuthorizationController(AuthorizationService authorizationService) {
        this.authorizationService = authorizationService;
    }

    @GetMapping("/crud/authorizations")
    public ResponseEntity<?> getActivityJournals(){
        return ResponseEntity.ok(authorizationService.getAll());
    }

    @GetMapping("/crud/authorization/{id}")
    public ResponseEntity<?> findAuthorizationByID(@PathVariable long id){
        return ResponseEntity.ok(authorizationService.findByID(id));
    }

    @DeleteMapping("/crud/deleteAuthorization/{id}")
    public void deleteAuthorizationByID(@PathVariable long id){
        authorizationService.deleteByID(id);
    }

    @RequestMapping(value = "/crud/updateAuthorization/{id}/{username}", method = RequestMethod.GET)
    public void updateActivityJournalByID(@PathVariable("id") long id, @PathVariable("username") String username){
        authorizationService.updateByID(id, username);
    }
}

