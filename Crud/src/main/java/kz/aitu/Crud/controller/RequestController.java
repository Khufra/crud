package kz.aitu.Crud.controller;

import kz.aitu.Crud.service.RequestService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class RequestController {
    private final RequestService requestService;

    public RequestController(RequestService requestService) {
        this.requestService = requestService;
    }

    @GetMapping("/crud/requests")
    public ResponseEntity<?> getRequests() {
        return ResponseEntity.ok(requestService.getAll());
    }

    @GetMapping("/crud/request/{id}")
    public ResponseEntity<?> findRequestByID(@PathVariable long id){
        return ResponseEntity.ok(requestService.findByID(id));
    }

    @DeleteMapping("/crud/deleteRequest/{id}")
    public void deleteRequestByID(@PathVariable long id){
        requestService.deleteByID(id);
    }

    @RequestMapping(value = "/crud/updateRequest/{id}/{requestUserId}", method = RequestMethod.GET)
    public void updateRequestByID(@PathVariable("id") long id, @PathVariable("requestUserId") long requestUserId){
        requestService.updateByID(id, requestUserId);
    }
}

