package kz.aitu.Crud.controller;

import kz.aitu.Crud.service.CaseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CaseController {
    private final CaseService caseService;

    public CaseController(CaseService caseService) {
        this.caseService = caseService;
    }

    @GetMapping("/crud/cases")
    public ResponseEntity<?> getCases() {
        return ResponseEntity.ok(caseService.getAll());
    }

    @GetMapping("/crud/case/{id}")
    public ResponseEntity<?> findCaseByID(@PathVariable long id){
        return ResponseEntity.ok(caseService.findByID(id));
    }

    @DeleteMapping("/crud/deleteCase/{id}")
    public void deleteCaseByID(@PathVariable long id){
        caseService.deleteByID(id);
    }

    @RequestMapping(value = "/crud/updateCase/{id}/{caseHeadingEN}", method = RequestMethod.GET)
    public void updateCaseByID(@PathVariable("id") long id, @PathVariable("caseHeadingEN") String caseHeadingEN){
        caseService.updateByID(id, caseHeadingEN);
    }
}

