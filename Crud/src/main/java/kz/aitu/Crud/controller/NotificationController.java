package kz.aitu.Crud.controller;

import kz.aitu.Crud.service.NotificationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class NotificationController {
    private final NotificationService notificationService;

    public NotificationController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @GetMapping("/crud/notifications")
    public ResponseEntity<?> getNotifications() {
        return ResponseEntity.ok(notificationService.getAll());
    }

    @GetMapping("/crud/notification/{id}")
    public ResponseEntity<?> findNotificationByID(@PathVariable long id){
        return ResponseEntity.ok(notificationService.findByID(id));
    }

    @DeleteMapping("/crud/deleteNotification/{id}")
    public void deleteNotificationByID(@PathVariable long id){
        notificationService.deleteByID(id);
    }

    @RequestMapping(value = "/crud/updateNotification/{id}/{objectType}", method = RequestMethod.GET)
    public void updateNotificationByID(@PathVariable("id") long id, @PathVariable("objectType") String objectType){
        notificationService.updateByID(id, objectType);
    }
}

