package kz.aitu.Crud.controller;

import kz.aitu.Crud.service.SearchKeyRoutingService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class SearchKeyRoutingController {
    private final SearchKeyRoutingService searchKeyRoutingService;

    public SearchKeyRoutingController(SearchKeyRoutingService searchKeyRoutingService) {
        this.searchKeyRoutingService = searchKeyRoutingService;
    }

    @GetMapping("/crud/searchKeyRoutings")
    public ResponseEntity<?> getSearchKeyRoutings() {
        return ResponseEntity.ok(searchKeyRoutingService.getAll());
    }

    @GetMapping("/crud/searchKeyRouting/{id}")
    public ResponseEntity<?> findSearchKeyRoutingByID(@PathVariable long id){
        return ResponseEntity.ok(searchKeyRoutingService.findByID(id));
    }

    @DeleteMapping("/crud/deleteSearchKeyRouting/{id}")
    public void deleteSearchKeyRoutingByID(@PathVariable long id){
        searchKeyRoutingService.deleteByID(id);
    }

    @RequestMapping(value = "/crud/updateSearchKeyRouting/{id}/{tableName}", method = RequestMethod.GET)
    public void updateSearchKeyRoutingByID(@PathVariable("id") long id, @PathVariable("tableName") String tableName){
        searchKeyRoutingService.updateByID(id, tableName);
    }
}

