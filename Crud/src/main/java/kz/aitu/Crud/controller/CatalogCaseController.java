package kz.aitu.Crud.controller;

import kz.aitu.Crud.service.CatalogCaseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CatalogCaseController {
    private final CatalogCaseService catalogCaseService;

    public CatalogCaseController(CatalogCaseService catalogCaseService) {
        this.catalogCaseService = catalogCaseService;
    }

    @GetMapping("/crud/catalogCases")
    public ResponseEntity<?> getCatalogCases() {
        return ResponseEntity.ok(catalogCaseService.getAll());
    }

    @GetMapping("/crud/catalogCase/{id}")
    public ResponseEntity<?> findCatalogCaseByID(@PathVariable long id){
        return ResponseEntity.ok(catalogCaseService.findByID(id));
    }

    @DeleteMapping("/crud/deleteCatalogCase/{id}")
    public void deleteCatalogCaseByID(@PathVariable long id){
        catalogCaseService.deleteByID(id);
    }

    @RequestMapping(value = "/crud/updateCatalogCase/{id}/{createdBy}", method = RequestMethod.GET)
    public void updateCatalogCaseByID(@PathVariable("id") long id, @PathVariable("createdBy") long createdBy){
        catalogCaseService.updateByID(id, createdBy);
    }
}

