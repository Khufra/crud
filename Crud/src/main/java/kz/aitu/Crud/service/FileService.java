package kz.aitu.Crud.service;


import kz.aitu.Crud.repository.FileRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class FileService {
    private final FileRepository fileRepository;

    public FileService(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(fileRepository.findAll());
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(fileRepository.findById(id));
    }

    public void deleteByID(long id){
        fileRepository.deleteById(id);
    }

    public void updateByID(long id, String name){
        fileRepository.updateNameByID(name, id);
    }
}

