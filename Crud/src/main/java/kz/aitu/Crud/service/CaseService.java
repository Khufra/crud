package kz.aitu.Crud.service;


import kz.aitu.Crud.repository.CaseRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CaseService {
    private final CaseRepository caseRepository;

    public CaseService(CaseRepository caseRepository) {
        this.caseRepository = caseRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(caseRepository.findAll());
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(caseRepository.findById(id));
    }

    public void deleteByID(long id){
        caseRepository.deleteById(id);
    }

    public void updateByID(long id, String caseHeadingEN){
        caseRepository.updateCaseHeadingEnByID(caseHeadingEN, id);
    }
}

