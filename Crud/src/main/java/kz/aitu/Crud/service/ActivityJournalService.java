package kz.aitu.Crud.service;

import kz.aitu.Crud.model.ActivityJournal;
import kz.aitu.Crud.repository.ActivityJournalRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class ActivityJournalService {
    private final ActivityJournalRepository activityJournalRepository;

    public ActivityJournalService(ActivityJournalRepository activityJournalRepository) {
        this.activityJournalRepository = activityJournalRepository;
    }

    public ResponseEntity<?> getActivityJournals(){
        return ResponseEntity.ok(activityJournalRepository.findAll());
    }

    public ResponseEntity<?> findActivityJournalByID(long id){
        return ResponseEntity.ok(activityJournalRepository.findById(id));
    }

    public void deleteActivityJournalByID(long id){
        activityJournalRepository.deleteById(id);
    }

    public void updateActivityJournalByID(long id, String eventType){
        activityJournalRepository.updateEventTypeByID(eventType, id);
    }

    public ActivityJournal create(ActivityJournal activityJournal){
        return activityJournalRepository.save(activityJournal);
    }
}

