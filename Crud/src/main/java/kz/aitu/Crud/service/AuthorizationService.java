package kz.aitu.Crud.service;

import kz.aitu.Crud.repository.AuthorizationRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

    @Service
    public class AuthorizationService {
        private final AuthorizationRepository authorizationRepository;

        public AuthorizationService(AuthorizationRepository authorizationRepository) {
            this.authorizationRepository = authorizationRepository;
        }

        public ResponseEntity<?> getAll(){
            return ResponseEntity.ok(authorizationRepository.findAll());
        }

        public ResponseEntity<?> findByID(long id){
            return ResponseEntity.ok(authorizationRepository.findById(id));
        }

        public void deleteByID(long id){
            authorizationRepository.deleteById(id);
        }

        public void updateByID(long id, String username){
            authorizationRepository.updateUsernameByID(username, id);
        }
    }


