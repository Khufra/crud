package kz.aitu.Crud.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "searchkeyrouting")
public class SearchKeyRouting {
    @Id
    private long id;
    private long searchkeyid;
    private String tablename;
    private long tableid;
    private String type;
}

