ALTER TABLE Users
    ADD CONSTRAINT fk1 FOREIGN KEY (authId) REFERENCES authorization(id);

ALTER TABLE "companyUnit"
    ADD CONSTRAINT fk1 FOREIGN KEY (companyid) REFERENCES company(id);

ALTER TABLE company
    ADD CONSTRAINT fk1 FOREIGN KEY (fondid) REFERENCES fond(id);

ALTER TABLE record
    ADD CONSTRAINT fk1 FOREIGN KEY (companyunitid) REFERENCES "companyUnit"(id);

ALTER TABLE share
    ADD CONSTRAINT fk1 FOREIGN KEY (requestid) REFERENCES request(id);

ALTER TABLE "historyRequestStatus"
    ADD CONSTRAINT fk1 FOREIGN KEY (requestid) REFERENCES request(id);

ALTER TABLE "catalogCase"
    ADD CONSTRAINT fk1 FOREIGN KEY (catalogid) REFERENCES catalog(id);

ALTER TABLE case
    ADD CONSTRAINT fk1 FOREIGN KEY (locationid) REFERENCES location(id);

ALTER TABLE "catalogCase"
    ADD CONSTRAINT fk2 FOREIGN KEY (caseid) REFERENCES case(id);

ALTER TABLE "catalogCase"
    ADD CONSTRAINT fk3 FOREIGN KEY (companyunitid) REFERENCES "companyUnit"(id);

ALTER TABLE "fileRouting"
    ADD CONSTRAINT fk1 FOREIGN KEY (tableid) REFERENCES case(id);

ALTER TABLE request
    ADD CONSTRAINT fk1 FOREIGN KEY (caseid) REFERENCES case(id);

ALTER TABLE request
    ADD CONSTRAINT fk2 FOREIGN KEY (caseindexid) REFERENCES "caseIndex"(id);

ALTER TABLE "fileRouting"
    ADD CONSTRAINT fk2 FOREIGN KEY (fileid) REFERENCES file(id);

ALTER TABLE file
    ADD CONSTRAINT fk1 FOREIGN KEY (filebinaryid) REFERENCES "tempFiles"(id);

ALTER TABLE "case"
    ADD CONSTRAINT fk2 FOREIGN KEY (structuralsubdivisionid) REFERENCES "companyUnit"(id);

ALTER TABLE case
    ADD CONSTRAINT fk3 FOREIGN KEY (destructionactid) REFERENCES "destructionAct"(id);

ALTER TABLE case
    ADD CONSTRAINT fk4 FOREIGN KEY (inventoryid) REFERENCES record(id);

ALTER TABLE case
    ADD CONSTRAINT fk5 FOREIGN KEY (caseindexid) REFERENCES "caseIndex"(id);

ALTER TABLE "nomenclatureSummary"
    ADD CONSTRAINT fk1 FOREIGN KEY (companyunitid) REFERENCES "companyUnit"(id);

ALTER TABLE users
    ADD CONSTRAINT fk2 FOREIGN KEY (companyunitid) REFERENCES "companyUnit"(id);

ALTER TABLE request
    ADD CONSTRAINT fk3 FOREIGN KEY (companyunitid) REFERENCES "companyUnit"(id);

ALTER TABLE nomenclature
    ADD CONSTRAINT fk1 FOREIGN KEY (companyunitid) REFERENCES "companyUnit"(id);

ALTER TABLE "searchKey"
    ADD CONSTRAINT fk1 FOREIGN KEY (companyunitid) REFERENCES "companyUnit"(id);

ALTER TABLE "searchKeyRouting"
    ADD CONSTRAINT fk1 FOREIGN KEY (searchkeyid) REFERENCES "searchKey"(id);

ALTER TABLE "searchKeyRouting"
    ADD CONSTRAINT fk2 FOREIGN KEY (tableid) REFERENCES "case"(id);

ALTER TABLE "destructionAct"
    ADD CONSTRAINT fk1 FOREIGN KEY (structuralsubdivisionid) REFERENCES "companyUnit"(id);

ALTER TABLE "caseIndex"
    ADD CONSTRAINT fk1 FOREIGN KEY (companyunitid) REFERENCES "companyUnit"(id);
